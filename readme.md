[![Contribute](http://codenvy.com/factory/resources/codenvy-contribute.svg)](https://codenvy.com/f?id=9m1lz5zlf0noijb8)

# Spring PetClinic Sample Application

## What does it look like?
-spring-petclinic has been deployed here on cloudfoundry: http://demo-spring-petclinic.cfapps.io/


## Understanding the Spring Petclinic application with a few diagrams
<a href="https://speakerdeck.com/michaelisvy/spring-petclinic-sample-application">See the presentation here</a>

## Running petclinic locally
```
	git clone https://github.com/SpringSource/spring-petclinic.git
	mvn tomcat7:run
```

You can then access petclinic here: http://localhost:9966/petclinic/

## In case you find a bug/suggested improvement for Spring Petclinic
Our issue tracker is available here: https://github.com/SpringSource/spring-petclinic/issues

## Working with Petclinic in Eclipse/STS

### prerequisites
The following items should be installed in your system:
* Maven 3 (http://www.sonatype.com/books/mvnref-book/reference/installation.html)
* git command line tool (https://help.github.com/articles/set-up-git)
* Eclipse with the m2e plugin (m2e is installed by default when using the STS (http://www.springsource.org/sts) distribution of Eclipse)

Note: when m2e is available, there is an m2 icon in Help -> About dialog.
If m2e is not there, just follow the install process here: http://eclipse.org/m2e/download/


### Steps:

1) In the command line
```
git clone https://github.com/SpringSource/spring-petclinic.git
```
2) Inside Eclipse
```
File -> Import -> Maven -> Existing Maven project
```


## Looking for something in particular?

| Inside the 'Web' layer | Files |
| ---------------------- | ----- |
| Spring MVC- Atom integration | [VetsAtomView.java](src/main/java/org/springframework/samples/petclinic/web/VetsAtomView.java) [mvc-view-config.xml](src/main/resources/spring/mvc-view-config.xml) |
| Spring MVC - XML integration | [mvc-view-config.xml](src/main/resources/spring/mvc-view-config.xml) |
| Spring MVC - ContentNegotiatingViewResolver | [mvc-view-config.xml](src/main/resources/spring/mvc-view-config.xml) |
| Spring MVC Test Framework | [VisitsViewTest.java](src/test/java/org/springframework/samples/petclinic/web/VisitsViewTests.java) |
| JSP custom tags | [WEB-INF/tags](src/main/webapp/WEB-INF/tags) [createOrUpdateOwnerForm.jsp](src/main/webapp/WEB-INF/jsp/owners/createOrUpdateOwnerForm.jsp) |
| webjars | [webjars declaration inside pom.xml](/pom.xml) [Resource mapping in Spring configuration](src/main/resources/spring/mvc-core-config.xml#L24) [sample usage in JSP](src/main/webapp/WEB-INF/jsp/fragments/headTag.jsp#L12) |
| Dandelion-datatables | [ownersList.jsp](src/main/webapp/WEB-INF/jsp/owners/ownersList.jsp) [vetList.jsp](src/main/webapp/WEB-INF/jsp/vets/vetList.jsp) [web.xml](src/main/webapp/WEB-INF/web.xml) [datatables.properties](/src/main/resources/dandelion/datatables/datatables.properties) |
| Thymeleaf branch | [See here](http://www.thymeleaf.org/petclinic.html) |
| Branch using GemFire and Spring Data GemFire instead of ehcache (thanks Bijoy Choudhury) | [See here](https://github.com/bijoych/spring-petclinic-gemfire) |

| 'Service' and 'Repository' layers | Files |
| :-------------------------------- | ----- |
| Transactions | [business-config.xml](src/main/resources/spring/business-config.xml) [ClinicServiceImpl.java](src/main/java/org/springframework/samples/petclinic/service/ClinicServiceImpl.java) |
| Cache | [tools-config.xml](src/main/resources/spring/tools-config.xml) [ClinicServiceImpl.java](src/main/java/org/springframework/samples/petclinic/service/ClinicServiceImpl.java) |
| Bean Profiles | [business-config.xml](src/main/resources/spring/business-config.xml) [ClinicServiceJdbcTests.java](src/test/java/org/springframework/samples/petclinic/service/ClinicServiceJdbcTests.java) [web.xml](src/main/webapp/WEB-INF/web.xml) |
| JdbcTemplate | [business-config.xml](src/main/resources/spring/business-config.xml) [jdbc folder](src/main/java/org/springframework/samples/petclinic/repository/jdbc) |
| JPA | [business-config.xml](src/main/resources/spring/business-config.xml) [jpa folder](src/main/java/org/springframework/samples/petclinic/repository/jpa) |
| Spring Data JPA | [business-config.xml](src/main/resources/spring/business-config.xml) [springdatajpa folder](src/main/java/org/springframework/samples/petclinic/repository/springdatajpa) |

| Others | Files |
| :----- | :---- |
| Gradle branch | [See here](https://github.com/whimet/spring-petclinic) |

## Interaction with other open source projects

One of the best parts about working on the Spring Petclinic application is that we have the opportunity to work in direct contact with many Open Source projects. We found some bugs/suggested improvements on various topics such as Spring, Spring Data, Bean Validation and even Eclipse! In many cases, they've been fixed/implemented in just a few days.
Here is a list of them:

|  Name | Issue |
| :---- | :---- |
| Spring JDBC: simplify usage of NamedParameterJdbcTemplate | [SPR-10256](https://jira.springsource.org/browse/SPR-10256) and [SPR-10257](https://jira.springsource.org/browse/SPR-10257) |
| Bean Validation / Hibernate Validator: simplify Maven dependencies and backward compatibility | [HV-790](https://hibernate.atlassian.net/browse/HV-790) and [HV-792](https://hibernate.atlassian.net/browse/HV-792) |
| Spring Data: provide more flexibility when working with JPQL queries | [DATAJPA-292](https://jira.springsource.org/browse/DATAJPA-292) |
| Eclipse: validation bug when working with .tag/.tagx files (has only been fixed for Eclipse 4.3 (Kepler)). [See here for more details](https://github.com/spring-projects/spring-petclinic/issues/14). | [STS-3294](https://issuetracker.springsource.com/browse/STS-3294) |